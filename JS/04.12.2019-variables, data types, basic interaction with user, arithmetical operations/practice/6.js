/**
 * ЗАДАНИЕ 6
 *
 * При помощи модального окна prompt получить от пользователя два числа.
 * Вывести в консоль сумму, разницу, произведение, результат деления и остаток от деления их друг на друга.
 */

const firstNumber = prompt('Enter the first number', '13');
const secondNumber = prompt('Enter the second number', '16');

console.log(firstNumber + secondNumber);
console.log(firstNumber - secondNumber);
console.log(firstNumber / secondNumber);
console.log(firstNumber % secondNumber);
